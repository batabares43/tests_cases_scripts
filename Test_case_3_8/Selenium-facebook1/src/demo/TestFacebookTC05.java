package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import dto.DatosUsuarioTestCase04;
import dto.DatosUsuarioTestCase05;

public class TestFacebookTC05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//REGISTRO DE NUEVO USUARIO CORRECTAMENTE
		
		
		try {
			Leer_Fichero_TestCase05 accediendo = new Leer_Fichero_TestCase05();
			List<DatosUsuarioTestCase05> lista = accediendo.lee();
			
			for (DatosUsuarioTestCase05 datosUsuario : lista) {
			    // hacer el TES
			    String path = 	"C:\\\\Users\\\\CRIVERA\\\\Documents\\\\Nueva carpeta\\\\geckodriver-v0.29.0-win64\\\\geckodriver.exe";
				//String filePath = "C:\\\\Users\\\\CRIVERA\\\\Pictures\\\\Porvenir\\\\qua.jpg";
				System.setProperty("webdriver.gecko.driver", path);
				WebDriver driver = new  FirefoxDriver();
				
				driver.get("https://www.facebook.com/r.php?next=https%3A%2F%2Fdevelopers.facebook.com%2F%3Flocale%3Des_ES&locale=es_LA&display=page");
				driver.manage().window().maximize();
				driver.findElement(By.xpath(".//*[@name='firstname']")).sendKeys(datosUsuario.getFirstName());
				driver.findElement(By.xpath(".//*[@name='lastname']")).sendKeys(datosUsuario.getLastname());
				driver.findElement(By.xpath(".//*[@name='reg_email__']")).sendKeys(datosUsuario.getEmail());
				driver.findElement(By.xpath(".//*[@name='reg_passwd__']")).sendKeys(datosUsuario.getPassword());
				driver.navigate().back();
				
				
				driver.findElement(By.xpath(".//*[@name='birthday_day']")).sendKeys("30");
				driver.findElement(By.xpath(".//*[@name='birthday_month']")).sendKeys("ene");
				driver.findElement(By.xpath(".//*[@name='birthday_year']")).sendKeys("1999");
				
				driver.findElement(By.xpath(".//*[@name='sex']")).click();
				driver.findElement(By.xpath(".//*[@name='websubmit']")).click();
				

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}

}
class Leer_Fichero_TestCase05 {
	private static final String RUTA_ARCHIVO = "C:\\\\Users\\\\CRIVERA\\\\Desktop\\\\pruebasSoftware\\\\TestCase\\\\\\\\testCase05.txt";

	public List<DatosUsuarioTestCase05> lee() throws IOException {		

			BufferedReader  reader = new BufferedReader(new FileReader(RUTA_ARCHIVO));
			String line; 
			List<DatosUsuarioTestCase05> lista = new ArrayList<>();
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(",");
                String firstname=parts[0];
                String lastname=parts[1];
                String email=parts[2];
                String password=parts[3];
                DatosUsuarioTestCase05 usuarioTc05 = new DatosUsuarioTestCase05(firstname,lastname,email,password);
            	lista.add(usuarioTc05);
			}	
		return lista;	      
	}
}

