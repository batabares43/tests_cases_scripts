﻿using System;
using System.IO;

namespace Test_case_1_2
{
    class Program
    {
        private static FacebookAutomationTest facebookAutomation;

        static void Main(string[] args)
        {
            string[] lines = File.ReadAllLines("input.txt");
            facebookAutomation = new FacebookAutomationTest();
            facebookAutomation.EMail = lines[0];
            facebookAutomation.Password = lines[1];
            facebookAutomation.PostText = lines[2];
            facebookAutomation.loginToFB();
            facebookAutomation.preparePostToFB();
            facebookAutomation.publishPost();
        }
    }
}
