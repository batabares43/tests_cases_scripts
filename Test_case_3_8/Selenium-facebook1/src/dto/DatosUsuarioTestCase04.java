package dto;

public class DatosUsuarioTestCase04 {
	private String usuario;
	private String clave;
	
	
	
	public DatosUsuarioTestCase04 (String usuario, String clave) {
		this.usuario = usuario;
		this.clave = clave;
	}
	public String getUsuario() {
		return usuario;
	}
	public String getClave() {
		return clave;
	}
	@Override
	public String toString() {
		return "DatosUsuarioTestCase04 [usuario=" + usuario + ", clave=" + clave + "]";
	}
}
