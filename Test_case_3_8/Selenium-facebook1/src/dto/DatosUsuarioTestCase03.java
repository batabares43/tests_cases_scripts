package dto;

public class DatosUsuarioTestCase03 {
	private String usuario;
	private String clave;
	
	
	
	public DatosUsuarioTestCase03 (String usuario, String clave) {
		this.usuario = usuario;
		this.clave = clave;
	}
	public String getUsuario() {
		return usuario;
	}
	public String getClave() {
		return clave;
	}
	@Override
	public String toString() {
		return "DatosUsuarioTestCase03 [usuario=" + usuario + ", clave=" + clave + "]";
	}
	
}
