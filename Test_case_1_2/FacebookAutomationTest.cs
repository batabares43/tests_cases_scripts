﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_case_1_2
{
    public class FacebookAutomationTest
    {

        private OpenQA.Selenium.IWebDriver driver;
        private PageFacebookLogin pageFacebookLogin;
        private PageFacebookWall pageFacebookWall;

        
        public FacebookAutomationTest()
        {
            OpenQA.Selenium.Chrome.ChromeOptions options = new OpenQA.Selenium.Chrome.ChromeOptions();
            options.AddArguments(new string[] { "--start-maximized", "--disable-notifications", "--incognito" });
            driver = new OpenQA.Selenium.Chrome.ChromeDriver(options);
            
        }

        ~FacebookAutomationTest()
        {
            if ( !( driver is null ) )
            {
                driver.Quit();
            }
        }
        

        
        private string email;

		public string EMail
		{
			get { return email; }
			set { email = value; }
		}

		private string password;

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		private string postText;

		public string PostText
		{
			get { return postText; }
			set { postText = value; }
		}

		
        


		public void loginToFB()
		{
            if ( email is null || password is null )
            {
                throw new Exception( "You must initialize Facebook email and password." );
            }

			driver.Navigate().GoToUrl( String.Format( "https://www.facebook.com/" ) );
			

            pageFacebookLogin = new PageFacebookLogin( driver );
            pageFacebookLogin.Login( email, password );
		}

        public void preparePostToFB()
        {
            pageFacebookWall = new PageFacebookWall( driver);
            pageFacebookWall.preparePostToFB( postText);
        }

        public void publishPost()
        {
            pageFacebookWall.publishPost();
        }
    }
}
