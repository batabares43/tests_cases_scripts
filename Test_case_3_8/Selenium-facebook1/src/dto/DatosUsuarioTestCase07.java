package dto;

public class DatosUsuarioTestCase07 {
private String email;

public DatosUsuarioTestCase07(String email) {
	super();
	this.email = email;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

@Override
public String toString() {
	return "DatosUsuarioTestCase07 [email=" + email + "]";
}

}
