#!usr/bin/env python
#_*_ coding: utf-8 _*_

from selenium.webdriver import Chrome
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.remote_connection import LOGGER

import time, sys, logging, getpass, os, unittest, pyautogui

class BuscarPerfil(unittest.TestCase):

    def setUp(self):
        
        options = webdriver.Options()

        options.add_argument("--start-maximized")

        options.add_argument("--log-level=OFF")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-setuid-sandbox")
        options.add_argument("--disable-webgl")
        options.add_argument("--disable-popup-blocking")
        options.add_argument("--disable-infobars")
        options.add_argument('--disable-logging')
        options.add_argument("--ignore-certificate-errors")
        options.add_argument('--ignore-ssl-errors')
        options.add_argument('--ignore-certificate-errors-spki-list')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])

        prefs = {'profile.default_content_setting_values.notifications': 2,
                'safebrowsing.enabled': True,
                'safebrowsing.disable_download_protection': False}

        options.add_experimental_option('prefs', prefs)
            
        LOGGER.setLevel(logging.WARNING)
            
        self.url = "https://es-la.facebook.com/"
        
        with open("parametros_TC.txt", "r") as file:
            
            data = file.readlines()
            
            self.user = data[1].split(",")[0]
                    
            self.password = data[1].split(",")[1]
                
            self.busqueda = data[1].split(",")[2].replace("\n", "")
            
            file.close()
        
        self.driver = Chrome(executable_path="chromedriver.exe", options=options, service_log_path='NUL')

        print("\n [+] Ejecutando la prueba...\n")

    def test_buscar_perfil(self):
        
        driver = self.driver
        
        url = self.url
        
        user = self.user
        
        password = self.password
        
        busqueda = self.busqueda
        
        driver.get(url)
        
        self.assertIn("Facebook - Inicia sesión o regístrate", driver.title)
        
        time.sleep(2)
        
        driver.find_element_by_xpath('//*[@id="email"]').send_keys(user)
        
        driver.find_element_by_xpath('//*[@id="pass"]').send_keys(password)
        
        driver.find_element_by_name('login').click()
        
        time.sleep(4)
        
        driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[2]/div[2]/div/div/div/div/div/label/input').send_keys(busqueda)

        time.sleep(3)
        
        pyautogui.press('enter')

        time.sleep(3)
        
        self.assertIn(self.busqueda, driver.title)
        
        driver.execute_script("window.scrollBy(0, 1200)", "")

        time.sleep(10)
        
        driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[2]/div[4]/div[1]/span').click()
        
        time.sleep(3)
        
        driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div[2]/div[4]/div[2]/div/div/div[1]/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div[3]/div/div[4]/div/div[1]/div[2]/div/div/div/div/span').click()
        
        time.sleep(4)
        
    def tearDown(self):
        
        self.driver.close()
    
def main():
    
    os.system("cls")
    
    print("\n   PRUEBA AUTOMATIZADA EN FACEBOOK - BUSQUEDA DE PERFIL\n")
    
    unittest.main()
        
    print("\n [+] Prueba terminada...")
    
if __name__ == "__main__": main() 