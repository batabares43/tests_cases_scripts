package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import dto.DatosUsuarioTestCase06;
import dto.DatosUsuarioTestCase07;

public class TestFacebookTC07 {
	public static void main(String[]args) {
		
		//CAMBIO DE CONTRASEŅA CORRECTO
		
	
		try {
			Leer_Fichero_TestCase07 accediendo = new Leer_Fichero_TestCase07();
			List<DatosUsuarioTestCase07> lista = accediendo.lee();
			
			for (DatosUsuarioTestCase07 datosUsuario : lista) {
			    // hacer el TES
			    String path = 	"C:\\\\Users\\\\CRIVERA\\\\Documents\\\\Nueva carpeta\\\\geckodriver-v0.29.0-win64\\\\geckodriver.exe";
				//String filePath = "C:\\\\Users\\\\CRIVERA\\\\Pictures\\\\Porvenir\\\\qua.jpg";
				System.setProperty("webdriver.gecko.driver", path);
				WebDriver driver = new  FirefoxDriver();
				
				driver.get("https://www.facebook.com/");
				driver.manage().window().maximize();
				
				driver.findElement(By.xpath(".//*[@class='_6ltj']")).click();
				  driver.findElement(By.xpath(".//*[@id='identify_email']")).sendKeys(datosUsuario.getEmail());
				driver.findElement(By.xpath(".//*[@id='did_submit']")).click();
				//
				//driver.findElement(By.xpath(".//*[@name='reset_action']")).click();
				//driver.findElement(By.xpath(".//*[@class='_42ft _4jy0 _9nq0 textPadding20px _4jy3 _4jy1 selected _51sy']")).click();
				//driver.findElement(By.xpath(".//*[@class='cometAccountRecoveryCard/loggedOutAccountAuxContent']")).click();
				driver.findElement(By.xpath(".//*[@class='_42ft _4jy0 _9nq0 textPadding20px _4jy3 _4jy1 selected _51sy']")).click();
				driver.findElement(By.xpath(".//*[@class='_42ft _4jy0 _9nq0 textPadding20px _4jy3 _4jy1 selected _51sy']")).click();
				
				//driver.findElement(By.xpath(".//*[@name='reset_action']")).click();
				//driver.findElement(By.xpath(".//*[@type='submit']")).click();
				
				//driver.navigate().back();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
class Leer_Fichero_TestCase07 {
	private static final String RUTA_ARCHIVO = "C:\\\\Users\\\\CRIVERA\\\\Desktop\\\\pruebasSoftware\\\\TestCase\\\\\\\\testCase07.txt";

	public List<DatosUsuarioTestCase07> lee() throws IOException {		

			BufferedReader  reader = new BufferedReader(new FileReader(RUTA_ARCHIVO));
			String line; 
			List<DatosUsuarioTestCase07> lista = new ArrayList<>();
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(",");
                String email=parts[0];
             
                DatosUsuarioTestCase07 usuarioTc07 = new DatosUsuarioTestCase07(email);
            	lista.add(usuarioTc07);
			}	
		return lista;	      
	}
}
