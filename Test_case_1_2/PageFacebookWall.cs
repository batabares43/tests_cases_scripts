﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Test_case_1_2
{
    class PageFacebookWall
    {
        private OpenQA.Selenium.IWebDriver driver;
        private OpenQA.Selenium.IWebElement elemPostText;
        private OpenQA.Selenium.IWebElement createButton;
        private OpenQA.Selenium.IWebElement postCreateButton;
       

        public PageFacebookWall(OpenQA.Selenium.IWebDriver driver)
        {
            this.driver = driver;
            
        }

     

        private void SetPostTextElement()
        {
            
                elemPostText = driver.FindElement( OpenQA.Selenium.By.XPath("/html/body/div[3]/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/div/div[2]/div/div/div/div"), 10 );
                elemPostText.Click();
            
        }

        public void preparePostToFB(string postText)
        {
            createButton = driver.FindElement(OpenQA.Selenium.By.XPath("/html/body/div[1]/div/div[1]/div/div[2]/div[4]/div[1]/div[3]/span/div"), 10);
            createButton.Click();
            postCreateButton = driver.FindElement(OpenQA.Selenium.By.XPath("/html/body/div[1]/div/div[1]/div/div[2]/div[4]/div[2]/div/div/div[1]/div[1]/div/div/div/div/div/div/div/div[2]/div[1]/div/div[1]/div[1]/div/i"), 10);
            postCreateButton.Click();
            if ( !postText.Equals( string.Empty ) )
            {
                SetPostTextElement();
                elemPostText.SendKeys( postText );
            }
        }

        public void publishPost()
        {
            OpenQA.Selenium.IWebElement elemPostButton = driver.FindElement( OpenQA.Selenium.By.XPath("/html/body/div[3]/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div/div[3]/div[2]/div/div"), 10 );
            elemPostButton.Click();
        }
    }
}
