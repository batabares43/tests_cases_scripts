package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import dto.DatosUsuarioTestCase03;


public class TestFacebookTC03 {
	public static void main(String[]args) {
		//INICIAR SESION CORRECTAMENTE 
		//
		
		try {
			Leer_Fichero_TestCase03 accediendo = new Leer_Fichero_TestCase03();
			List<DatosUsuarioTestCase03> lista = accediendo.lee();
			
			for (DatosUsuarioTestCase03 datosUsuario : lista) {
			    // hacer el TES
			    String path = 	"C:\\\\Users\\\\CRIVERA\\\\Documents\\\\Nueva carpeta\\\\geckodriver-v0.29.0-win64\\\\geckodriver.exe";
				//String filePath = "C:\\\\Users\\\\CRIVERA\\\\Pictures\\\\Porvenir\\\\qua.jpg";
				System.setProperty("webdriver.gecko.driver", path);
				WebDriver driver = new  FirefoxDriver();
				driver.get("https://www.facebook.com/");
				driver.findElement(By.xpath(".//*[@name='email']")).sendKeys(datosUsuario.getUsuario());
				driver.findElement(By.xpath(".//*[@name='pass']")).sendKeys(datosUsuario.getClave());
				driver.findElement(By.xpath(".//*[@name='login']")).click();
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
class Leer_Fichero_TestCase03 {
	private static final String RUTA_ARCHIVO = "C:\\\\Users\\\\CRIVERA\\\\Desktop\\\\pruebasSoftware\\\\TestCase\\\\\\\\testCase03.txt";

	public List<DatosUsuarioTestCase03> lee() throws IOException {		

			BufferedReader  reader = new BufferedReader(new FileReader(RUTA_ARCHIVO));
			String line; 
			List<DatosUsuarioTestCase03> lista = new ArrayList<>();
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(",");
                String usuario=parts[0];
                String clave=parts[1];
                DatosUsuarioTestCase03 usuarioTc03 = new DatosUsuarioTestCase03(usuario,clave);
            	lista.add(usuarioTc03);
			}	
		return lista;	      
	}
}
